import json
import os
from functools import wraps

import requests
from rest_framework import status
from rest_framework.response import Response


def authentication_required(function):
    """
        Decorator to be used for authentication purposes. This function is deprecated and instead the
        registry.perimissions.py should be used in the Django Rest API.

        Args:
            function (function): the function annotated with the authentication_required decorator

        Returns:
            bool: if the user is authenticated
    """

    @wraps(function)
    def is_authenticated(request, *args, **kwargs):
        token = args[0].data["access_token"]
        if token:
            user_data = validate_token(token=token)
            if user_data[0] == 200:
                user_data = json.loads(user_data[1])
                args[0].data["username"] = user_data["username"]
                args[0].data["user_id"] = user_data["user_id"]
                args[0].data["issuer"] = user_data["issuer"]
                return True
            else:
                return False
        else:
            msg = "Token is expired or user has not signed in"
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    return is_authenticated


def validate_token(token):
    """
    Function to validate a given token. If the token is valid, the API asks for user info so as to retrieve a
    permanent id. Some of the returned fields (if token is valid) are as presented below:

    - "sub": "cfbdc618-1d1e-4b1d-9d35-3d23f271abe3",
    - "email_verified": true,
    - "name": "Sissy Themeli",
    - "preferred_username": "sthemeli",
    - "given_name": "Sissy",
    - "family_name": "Themeli",
    - "email": "sthemeli@iit.demokritos.gr"

    Args
        request: http request
        oidc: open-id client

    Returns
        str: user's permanent id
    """
    try:
        host = os.environ[
            "DARE_LOGIN_PUBLIC_SERVICE_HOST"] if "DARE_LOGIN_PUBLIC_SERVICE_HOST" in os.environ else "localhost"
        port = os.environ["DARE_LOGIN_PUBLIC_SERVICE_PORT"] if "DARE_LOGIN_PUBLIC_SERVICE_PORT" in os.environ else 5001
        url = "http://{}:{}/validate-token".format(host, port)
        response = requests.post(url, data=json.dumps({"access_token": token}))
        return response.status_code, response.text
    except (ConnectionError, Exception) as e:
        print(e)
        return 500, "Could not validate token"
