import json
import logging
from os.path import exists, join

from django.contrib.auth.models import User
from django.core.exceptions import EmptyResultSet, ObjectDoesNotExist, MultipleObjectsReturned
from django.db.utils import IntegrityError
from django.http import JsonResponse, HttpResponse
from rest_framework import status
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from registry import utils
from registry.models import ExperimentGroup, Experiment, Run, Upload
from registry.permissions import IsAuthenticated
from registry.serializers import ExperimentGroupSerializer, ExperimentSerializer, RunSerializer, UploadSerializer, \
    UserSerializer
from registry.services import ExperimentGroupService, ExperimentService, RunService, UploadService, DownloadService

logger = logging.getLogger(__name__)


# Create your views here.
class ExperimentGroupView(viewsets.ModelViewSet):
    """
        ExperimentGroupView contains all the API methods to create, update, delete or retrieve ExperimentGroup objects
        from the DB
    """
    permission_classes = (IsAuthenticated,)
    lookup_field = "experiment_group_id"
    serializer_class = ExperimentGroupSerializer
    queryset = ExperimentGroup.objects.all()

    def create(self, request, *arg, **kwargs):
        """
            API endpoint to create a new experiment group. The only mandatory request parameter is the username.
            If experiment_group_name is not provided, it will be auto-generated based on the existing auto-generated
            groups of the user. The dare_platform parameter will be used in order to integrated multiple DARE platforms.
            Use the ExperimentGroupService to interact with the DB.

            Use <IP>:<port>/exec-registry/experiment-groups with POST request to register an exp. group in DB.

            Args
                request(HttpRequest): the POST request containing all the information provided by the client

            Returns
                HttpResponse: API response with the created ExperimentGroup object
        """
        # get data from request
        try:
            username = request.data["username"]
        except (KeyError, Exception)as e:
            logger.error("Error while reading request: {}".format(e))
            msg = "Please provide a user"
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        experiment_group_name = request.data["experiment_group_name"] if "experiment_group_name" \
                                                                         in request.data.keys() else None
        dare_platform = request.data[
            "dare_platform"] if "dare_platform" in request.data.keys() else utils.DEFAULT_DARE_PLATFORM

        # use ExperimentGroupService to create a new ExperimentGroup
        experiment_group_service = ExperimentGroupService()

        try:
            result = experiment_group_service.create_experiment_group(experiment_group_name, username,
                                                                      dare_platform)
            # check if an error message is returned from the service
            if isinstance(result, str):
                return Response(result, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
            else:
                serializer = ExperimentGroupSerializer(result, many=False, context={'request': request})
                return Response(serializer.data, status=status.HTTP_200_OK)
        except (IntegrityError, Exception) as e:
            logger.error("Error while saving experiment group: {}".format(e))
            msg = "Error while saving experiment group with name {}".format(experiment_group_name)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @action(methods=['post'], detail=False, url_name="update-exp-group", url_path="update-exp-group")
    def update_exp_group(self, request, *args, **kwargs):
        """
            API endpoint to update an already existing experiment group. When it is updated, the respective
            folder is also renamed. The mandatory fields that should be provided are: the username, the current
            experiment group name and the new name. Dare platform parameter gets a default value (0) if it is not
            provided. ExperimentGroupService is used to update the respective value in the DB and rename the directory.

            Endpoint: <IP>:<port>/exec-registry/experiment-groups/update-exp-group/ using POST request
        """
        # get data from request
        try:
            experiment_group_name = request.data["experiment_group_name"]
            username = request.data["username"]
            new_experiment_group_name = request.data["new_experiment_group_name"]
        except (KeyError, Exception) as e:
            logger.error("Exception while reading update request for experiment groups! {}".format(e))
            msg = "User, old and new experiment group names should be provided"
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        dare_platform = request.data[
            "dare_platform"] if "dare_platform" in request.data.keys() else utils.DEFAULT_DARE_PLATFORM

        # use ExperimentGroupService to update an ExperimentGroup
        experiment_group_service = ExperimentGroupService()

        try:
            result = experiment_group_service.update_experiment_group(new_experiment_group_name,
                                                                      dare_platform, username,
                                                                      experiment_group_name)
            if isinstance(result, str):
                return Response(result, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
            else:
                serializer = ExperimentGroupSerializer(result, many=False, context={'request': request})
                return Response(serializer.data, status=status.HTTP_200_OK)
        except (EmptyResultSet, ObjectDoesNotExist, MultipleObjectsReturned, Exception) as e:
            logger.error("Exception raised while updating experiment group. {}".format(e))
            msg = "Error while updating experiment group with name {}".format(request.data["experiment_group_name"])
        return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @action(methods=['delete'], detail=False, url_name="delete-exp-group", url_path="delete-exp-group")
    def delete_exp_group(self, request, *args, **kwargs):
        """
            API endpoint to delete an existing experiment group. Username and experiment group name are mandatory
            parameters.
            ExperimentGroupService is used to delete the respective DB entry and the group directory.

            API endpoint: <IP>:<port>/exec-registry/experiment-groups/delete-exp-group/ using DELETE request
        """
        try:
            # get data from request
            try:
                username = request.data["username"]
                experiment_group_name = request.data["experiment_group_name"]
            except (KeyError, Exception):
                msg = "Username and experiment group name should be provided"
                return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
            dare_platform = request.data[
                "dare_platform"] if "dare_platform" in request.data.keys() else utils.DEFAULT_DARE_PLATFORM

            # use ExperimentGroupService to delete an ExperimentGroup
            experiment_group_service = ExperimentGroupService()

            if experiment_group_service.delete_experiment_group(experiment_group_name, username, dare_platform):
                msg = "Experiment group with name {} is deleted".format(experiment_group_name)
                return Response(msg, status=status.HTTP_200_OK)
            else:
                msg = "Experiment group with name {} not found".format(request.data["experiment_group_name"])
                return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        except (EmptyResultSet, ObjectDoesNotExist, MultipleObjectsReturned):
            msg = "Error while deleting experiment group with name {}".format(request.data["experiment_group_name"])
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @action(methods=['get'], detail=False, url_name="byuser", url_path="byuser")
    def get_by_user(self, request, *args, **kwargs):
        """
            API endpoint to get all the experiment groups of a user. The only mandatory parameter is the username.
            ExperimentGroupService is used to retrieve the data from the DB.

            Endpoint: <IP>:<port>/exec-registry/experiment-groups/byuser?username=<username> using GET request
        """
        try:
            username = request.data["username"]
        except (KeyError, Exception):
            msg = "Please provide a user to get experiment groups"
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        dare_platform = request.query_params[
            "dare_platform"] if "dare_platform" in request.query_params.keys() else utils.DEFAULT_DARE_PLATFORM

        # use ExperimentGroupService to find ExperimentGroups
        experiment_group_service = ExperimentGroupService()

        try:
            experiment_groups = experiment_group_service.get_experiment_groups(username, dare_platform)
            serializer = ExperimentGroupSerializer(experiment_groups, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)
        except (EmptyResultSet, ObjectDoesNotExist, MultipleObjectsReturned):
            msg = "Error while retrieving experiments for user with token {}".format(username)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @action(methods=['get'], detail=False, url_name="byname", url_path="byname")
    def get_by_name(self, request):
        """
            API endpoint to retrieve a single experiment group by name. Experiment groups have a unique combination
            of username, name and dare_platform fields. Username and experiment group name are mandatory parameters
            and should be provided in the request. Dare platform gets a default value if it is not provided. You
            can change this default value in the utils.py file.
            Endpoint: <IP>:<port>/exec-registry/experiment-groups/byname?username=<username>&
                                                                         experiment_group_name=<name>
        """
        # get query params from get request
        try:
            username = request.data["username"]
            experiment_group_name = request.query_params["experiment_group_name"]
        except (KeyError, Exception):
            msg = "Please provide a user to get experiment groups"
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        # add default value in dare_platform if it is not provided
        dare_platform = request.data[
            "dare_platform"] if "dare_platform" in request.query_params.keys() else utils.DEFAULT_DARE_PLATFORM

        experiment_group_service = ExperimentGroupService()

        # get the requested experiment group
        try:
            experiment_group = experiment_group_service.get_experiment_group(experiment_group_name, dare_platform,
                                                                             username)
            serializer = ExperimentGroupSerializer(experiment_group, many=False, context={
                'request': request})
            return Response(serializer.data, status=status.HTTP_200_OK)
        except (EmptyResultSet, ObjectDoesNotExist, MultipleObjectsReturned):
            msg = "Error while retrieving experiment group with name {} for user with token {}".format(
                experiment_group_name,
                username)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class ExperimentView(viewsets.ModelViewSet):
    """
        View for Experiment model. Provides methods to create, update, delete or retrieve experiments from the DB
    """
    permission_classes = (IsAuthenticated,)
    lookup_field = "experiment_id"
    serializer_class = ExperimentSerializer
    queryset = Experiment.objects.all()

    def create(self, request, *args, **kwargs):
        """
            API endpoint to create a new experiment. Each experiment is unique based on the username and an experiment
            group. If user does not uses groups to organize the experiments, a default ExperimentGroup will be created
            in the DB but the group will not be used in the folder structure. This information is depicted in the
            group_to_be_used field in the Experiment class. It is mandatory to provide the username but the remaining
            possible parameters are optional. The experiment name is automatically generated if it is not given.
            ExperimentService class contains methods to interact with the DB and access the folder structure to
            create/update/delete directories.

            Usage: <IP>:<port>/exec-registry/experiments/
        """
        # get data from request
        try:
            username = request.data["username"]
        except (KeyError, Exception) as e:
            logger.error("Exception while reading experiment post request: {}".format(e))
            msg = "Please provide a user!"
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        experiment_name = request.data["experiment_name"] if "experiment_name" in request.data.keys() else None
        dare_platform = request.data[
            "dare_platform"] if "dare_platform" in request.data.keys() else utils.DEFAULT_DARE_PLATFORM
        experiment_group_name = request.data[
            "experiment_group_name"] if "experiment_group_name" in request.data.keys() \
            else utils.DEFAULT_EXPERIMENT_GROUP
        group_to_be_used = True if "experiment_group_name" in request.data.keys() else False

        experiment_service = ExperimentService()

        try:
            result = experiment_service.create_experiment(experiment_group_name, username, dare_platform,
                                                          group_to_be_used, experiment_name)
            if isinstance(result, str):
                return Response(result, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
            else:
                serializer = ExperimentSerializer(result, many=False, context={'request': request})
                return Response(serializer.data, status=status.HTTP_200_OK)
        except (IntegrityError, Exception) as e:
            logger.error("Exception while saving a new experiment {}".format(e))
            msg = "Error while saving experiment with name {} for user {}. Exception: {}".format(experiment_name,
                                                                                                 username, e)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @action(methods=['post'], detail=False, url_name="update-exp", url_path="update-exp")
    def update_experiment(self, request, *args, **kwargs):
        """
            API endpoint to update an existing experiment. The expected parameters are: the username and the
            current experiment name (these are mandatory) and also a new experiment and/or experiment group
            name. The new experiment name parameter is used so as to rename the experiment directory, while
            the new experiment group name is used so as to move the experiment under a different experiment
            group (move the directory and update the relationship in the DB). Dare platform and experiment
            group are optional fields. If experiment group is not provided the default experiment group
            name is used. However, if the experiment groups are used in the folder structure this parameter
            must be provided.

            Usage: <IP>:<port>/exec-registry/experiments/update-exp/ using POST request
        """
        # get data from request
        try:
            experiment_name = request.data["experiment_name"]
            username = request.data["username"]
        except (KeyError, Exception):
            msg = "Experiment name or user are missing from the request!"
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        experiment_group_name = request.data[
            "experiment_group_name"] if "experiment_group_name" in request.data.keys() \
            else utils.DEFAULT_EXPERIMENT_GROUP
        dare_platform = request.data[
            "dare_platform"] if "dare_platform" in request.data.keys() else utils.DEFAULT_DARE_PLATFORM

        new_experiment_group_name = request.data[
            "new_experiment_group_name"] if "new_experiment_group_name" in request.data.keys() else None
        new_experiment_name = request.data[
            "new_experiment_name"] if "new_experiment_name" in request.data.keys() else None

        experiment_service = ExperimentService()

        if not new_experiment_group_name and not new_experiment_name:
            msg = "Provide something to update"
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        try:
            result = experiment_service.update_experiment(username, dare_platform, experiment_group_name,
                                                          experiment_name, new_experiment_group_name,
                                                          new_experiment_name)
            if not isinstance(result, str):
                serializer = ExperimentSerializer(result, many=False, context={'request': request})
                return Response(serializer.data, status=status.HTTP_200_OK)
            else:
                return Response(result, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        except (EmptyResultSet, ObjectDoesNotExist, MultipleObjectsReturned):
            msg = "Error while updating experiment with name {} for user {}".format(experiment_name, username)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @action(methods=['delete'], detail=False, url_name="delete-experiment", url_path="delete-experiment")
    def delete_experiment(self, request, *args, **kwargs):
        """
            API endpoint to delete an experiment. Username and experiment name are expected as request parameters.
            Dare platform and experiment group name take default values if not provided.

            Usage: <IP>:<port>/exec-registry/experiments/delete-experiment using DELETE request
        """
        # get request data
        try:
            user_token = request.data["username"]
            experiment_name = request.data["experiment_name"]
        except (KeyError, Exception):
            msg = "User and/or experiment name are not provided"
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        experiment_group_name = request.data[
            "experiment_group_name"] if "experiment_group_name" in request.data.keys() \
            else utils.DEFAULT_EXPERIMENT_GROUP
        dare_platform = request.data[
            "dare_platform"] if "dare_platform" in request.data.keys() else utils.DEFAULT_DARE_PLATFORM

        experiment_service = ExperimentService()

        # delete experiment
        if experiment_service.delete_experiment(user_token, dare_platform, experiment_group_name, experiment_name):
            msg = "Experiment with name {} is deleted".format(experiment_name)
            return Response(msg, status=status.HTTP_200_OK)
        else:
            msg = "Error while deleting experiment with name {}".format(request.data["experiment_name"])
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @action(methods=['get'], detail=False, url_name="by_user", url_path="byuser")
    def get_by_user(self, request, *args, **kwargs):
        """
            API endpoint to retrieve all the experiment of a user in an experiment group. Username is a mandatory
            parameter, while experiment group name and dare platform fall to a default value if not provided.

            Usage: <IP>:<port>/exec-registry/experiments/byuser?username=<username> (GET request)
        """
        # get request data
        try:
            username = request.data["username"]
        except (KeyError, Exception):
            msg = "Please provide a user to get registered experiments"
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        experiment_group_name = request.query_params[
            "experiment_group_name"] if "experiment_group_name" in request.query_params.keys() \
            else utils.DEFAULT_EXPERIMENT_GROUP
        dare_platform = request.query_params[
            "dare_platform"] if "dare_platform" in request.query_params.keys() else utils.DEFAULT_DARE_PLATFORM

        try:
            experiment_service = ExperimentService()
            experiments = experiment_service.get_experiments(experiment_group_name, username, dare_platform)
            serializer = ExperimentSerializer(experiments, many=True, context={
                'request': request})
            return Response(serializer.data, status=status.HTTP_200_OK)
        except (EmptyResultSet, ObjectDoesNotExist, MultipleObjectsReturned):
            msg = "Error while retrieving experiments for user with token {}".format(username)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @action(methods=['get'], detail=False, url_name="by_name", url_path="byname")
    def get_by_name(self, request):
        """
            API endpoint to retrieve a single experiment based on the username, the experiment group and
            the name of the experiment. All this information should be provided in the request. Only the
            experiment group name can be skipped if it is not used in the folders structure and in this case
            the default value will be used. Similarly, for the dare platform.

            Usage: <IP>:<port>/exec-registry/experiments/byname?username=<name>&
                                                                experiment_name=<exp_name>&
                                                                experiment_group_name=<name> (GET request)
        """
        # get request data
        try:
            experiment_name = request.query_params["experiment_name"]
            username = request.data["username"]
        except (KeyError, Exception):
            msg = "Please provide a user to get registered experiments"
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        experiment_group_name = request.query_params[
            "experiment_group_name"] if "experiment_group_name" in request.query_params.keys() \
            else utils.DEFAULT_EXPERIMENT_GROUP
        dare_platform = request.query_params[
            "dare_platform"] if "dare_platform" in request.query_params.keys() else utils.DEFAULT_DARE_PLATFORM

        try:
            experiment_service = ExperimentService()
            experiment = experiment_service.get_experiment(username, dare_platform, experiment_group_name,
                                                           experiment_name)
            if experiment:
                serializer = ExperimentSerializer(experiment, many=False, context={
                    'request': request})
                return Response(serializer.data, status=status.HTTP_200_OK)
            else:
                msg = "Experiment with name {} for user {} does not exist".format(experiment_name, username)
                return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        except (EmptyResultSet, ObjectDoesNotExist, MultipleObjectsReturned):
            msg = "Error while retrieving experiment with name {} for user with token {}".format(experiment_name,
                                                                                                 username)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @action(methods=['get'], detail=False, url_name="by_group", url_path="bygroup")
    def get_by_experiment_group(self, request):
        """
            API endpoint, similar to the byuser endpoint. The only difference is that experiment group name
            is now a mandatory parameter.

            Usage: <IP>:<port>/exec-registry/experiments/bygroup?username=<username>&experiment_name=<exp_name>
                                                                 &experiment_group_name=<name> (GET request)
        """
        # get request data
        try:
            username = request.data["username"]
            experiment_group_name = request.query_params["experiment_group_name"]
        except (KeyError, Exception):
            msg = "Please provide the user and the experiment group name"
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        dare_platform = request.query_params[
            "dare_platform"] if "dare_platform" in request.query_params.keys() else utils.DEFAULT_DARE_PLATFORM

        try:
            experiment_service = ExperimentService()
            experiments = experiment_service.get_experiments(experiment_group_name, username, dare_platform)
            if experiments:
                serializer = ExperimentSerializer(experiments, many=True, context={
                    'request': request})
                return Response(serializer.data, status=status.HTTP_200_OK)
            else:
                msg = "Experiments for group {} and user {} does not exist".format(experiment_group_name, username)
                return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        except (EmptyResultSet, ObjectDoesNotExist, MultipleObjectsReturned):
            msg = "Error while retrieving experiments for group {} and user {}".format(username,
                                                                                       experiment_group_name)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class RunView(viewsets.ModelViewSet):
    """
        View for Run model. Provides API endpoints for creating, updating, deleting or retrieving runs from the DB
    """
    permission_classes = (IsAuthenticated,)
    lookup_field = "run_id"
    serializer_class = RunSerializer
    queryset = Run.objects.all()

    def create(self, request, *args, **kwargs):
        """
            API endpoint to create new run. All runs are organized under experiments, which in their turn
            are organized in groups - the latter is optional. In order to create a new experiment, you need
            to specify the user and the experiment name and experiment group (optionally). Additionally, other
            optional parameters are the dare platform and the run name. If the latter is not provided, it is
            generated automatically based on the existing runs of the user under the specific experiment.

            Usage: <IP>:<port>/exec-registry/runs/ using a POST request
        """
        # get request data
        try:
            username = request.data["username"]
            kind = request.data["kind"]
        except (KeyError, Exception):
            msg = "Provide the user, the run kind (test or official) the run and experiment names!"
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        run_name = request.data["run_name"] if "run_name" in request.data.keys() else None
        experiment_group_name = request.data[
            "experiment_group_name"] if "experiment_group_name" in request.data.keys() \
            else utils.DEFAULT_EXPERIMENT_GROUP
        experiment_name = request.data["experiment_name"] if "experiment_name" in request.data.keys() \
            else None
        dare_platform = request.data[
            "dare_platform"] if "dare_platform" in request.data.keys() else utils.DEFAULT_DARE_PLATFORM
        # check if experiment is already created
        experiment_service = ExperimentService()
        try:
            experiment = experiment_service.get_experiment(username, dare_platform, experiment_group_name,
                                                           experiment_name)
        except (EmptyResultSet, ObjectDoesNotExist, MultipleObjectsReturned, BaseException, Exception):
            msg = "Create an Experiment first for user {} and name {}!".format(username, experiment_name)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        try:
            run_service = RunService()
            run = run_service.create_run(username, kind, experiment, run_name)
            serializer = RunSerializer(run, many=False, context={'request': request})
            return Response(serializer.data, status=status.HTTP_200_OK)
        except (IntegrityError, Exception) as e:
            logger.error("Exception occurred: {}".format(e))
            msg = "Error while persisting Run for token {} and experiment {}".format(username, experiment_name)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @action(methods=['post'], detail=False, url_name="update-run", url_path="update-run")
    def update_run(self, request, *args, **kwargs):
        """
            API endpoint to update an existing run. Mandatory params: username, run name and experiment name.
            Mandatory fields:

            a) experiment_group_name (if used in the folder structure) and dare platform
            b) new run, experiment or experiment group: at least one should be provided. New run name is used
            to rename an existing run folder and update the relevant DB entry, while the other two are used
            if a run or an experiment folder should be moved under a different experiment or experiment group
            respectively. RunService class is used to interact with the DB and the shared file system.

            Usage: <IP>:<port>/exec-registry/runs/update-run/ using a POST request
        """
        # get request data
        try:
            username = request.data["username"]
            run_name = request.data["run_name"]
            experiment_name = request.data["experiment_name"]
        except (KeyError, Exception):
            msg = "Run and/or experiment name and/or user are missing from the request"
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        new_run_name = request.data["new_run_name"] if "new_run_name" in request.data.keys() else None
        new_experiment_name = request.data[
            "new_experiment_name"] if "new_experiment_name" in request.data.keys() else None
        new_experiment_group_name = request.data[
            "new_experiment_group_name"] if "new_experiment_group_name" in request.data.keys() else None
        experiment_group_name = request.data[
            "experiment_group_name"] if "experiment_group_name" in request.data.keys() \
            else utils.DEFAULT_EXPERIMENT_GROUP
        dare_platform = request.data[
            "dare_platform"] if "dare_platform" in request.data.keys() else utils.DEFAULT_DARE_PLATFORM

        try:
            # update folders and DB
            run_service = RunService()
            result = run_service.update_run(username, dare_platform, experiment_group_name, experiment_name, run_name,
                                            new_run_name, new_experiment_name, new_experiment_group_name)
            # return response
            if not isinstance(result, str):
                serializer = RunSerializer(result, many=False, context={'request': request})
                return Response(serializer.data, status=status.HTTP_200_OK)
            else:
                return Response(result, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        except (EmptyResultSet, ObjectDoesNotExist, MultipleObjectsReturned):
            msg = "Error while updating Run with name {} for user {} and experiment {}".format(run_name, username,
                                                                                               experiment_name)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @action(methods=['delete'], detail=False, url_name="delete-run", url_path="delete-run")
    def delete_run(self, request, *args, **kwargs):
        """
            API endpoint to delete an existing run. It is obligatory to provide the run and experiment names
            as well as the username. Experiment group name and dare platform are optional and default values
            are used if they are not provided. When a run is deleted, the respective folder is also removed,
            and experiment & group are checked if they are empty so as to delete them as well.
            Usage: <IP>:<port>/exec-registry/runs/delete-run/ using a DELETE request
        """
        # get data from request
        try:
            run_name = request.data["run_name"]
            username = request.data["username"]
            experiment_name = request.data["experiment_name"]
        except (KeyError, Exception):
            msg = "Missing information! Please provide the user, the run and experiment names!"
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        experiment_group_name = request.data[
            "experiment_group_name"] if "experiment_group_name" in request.data.keys() \
            else utils.DEFAULT_EXPERIMENT_GROUP
        dare_platform = request.data[
            "dare_platform"] if "dare_platform" in request.data.keys() else utils.DEFAULT_DARE_PLATFORM

        try:
            # delete run in DB and the relevant folder in shared file system
            run_service = RunService()
            run_service.delete_run(username, dare_platform, experiment_group_name, experiment_name, run_name)
            msg = "Run with name {} is successfully deleted".format(run_name)
            return Response(msg, status=status.HTTP_200_OK)
        except (EmptyResultSet, ObjectDoesNotExist, MultipleObjectsReturned, OSError):
            msg = "Error while deleting Run with name {}".format(run_name)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @action(methods=['get'], detail=False, url_name="by_exp", url_path="byexp")
    def get_by_experiment(self, request, *args, **kwargs):
        """
            API endpoint (GET request) to retrieve all runs under an experiment.
            Mandatory fields: username and experiment name
            Optional: experiment group name and dare platform
            Usage: <IP>:<port>/exec-registry/runs/byexp?username=<username>&experiment_name=<name>
        """
        # get query params from request
        try:
            username = request.data["username"]
            experiment_name = request.query_params["experiment_name"]
        except (KeyError, Exception):
            msg = "Please provide a user and an experiment name to get registered runs"
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        experiment_group_name = request.query_params[
            "experiment_group_name"] if "experiment_group_name" in request.query_params.keys() \
            else utils.DEFAULT_EXPERIMENT_GROUP
        dare_platform = request.query_params[
            "dare_platform"] if "dare_platform" in request.query_params.keys() else utils.DEFAULT_DARE_PLATFORM
        kind = request.query_params["kind"] if "kind" in request.query_params.keys() else None

        # get user runs for specific experiment
        try:
            run_service = RunService()
            runs = run_service.get_runs_from_scratch(username, dare_platform, experiment_group_name,
                                                     experiment_name, kind=kind)
            serializer = RunSerializer(runs, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)
        except (EmptyResultSet, ObjectDoesNotExist, MultipleObjectsReturned):
            msg = "Error while retrieving runs for user with token {}".format(username)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @action(methods=['get'], detail=False, url_name="by_name", url_path="byname")
    def get_by_name(self, request):
        """
            API endpoint (GET request) to get a single run based on its name, the experiment and the user.
            Optionally, experiment group and dare platform can be provided.
            Usage: <IP>:<port>/exec-registry/runs/byname?username=<username>&experiment_name=<name>&
                                                         run_name=<run_name>
        """
        # get query params from request
        try:
            run_name = request.query_params["run_name"]
            username = request.data["username"]
            experiment_name = request.query_params["experiment_name"]
        except (KeyError, Exception):
            msg = "Please provide a user, an experiment name and a run name"
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        dare_platform = request.query_params[
            "dare_platform"] if "dare_platform" in request.query_params.keys() else utils.DEFAULT_DARE_PLATFORM
        experiment_group_name = request.query_params[
            "experiment_group_name"] if "experiment_group_name" in request.query_params.keys() \
            else utils.DEFAULT_EXPERIMENT_GROUP

        # get run by name
        try:
            run_service = RunService()
            run = run_service.get_run(username, dare_platform, experiment_group_name, experiment_name, run_name)
            serializer = RunSerializer(run, many=False, context={'request': request})
            return Response(serializer.data, status=status.HTTP_200_OK)
        except (EmptyResultSet, ObjectDoesNotExist, MultipleObjectsReturned):
            msg = "Error while retrieving run with alias {} for user {}".format(run_name, username)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @action(methods=['get'], detail=False, url_name="by_user", url_path="byuser")
    def get_by_user(self, request, *args, **kwargs):
        """
            API endpoint to get all user runs, despite the experiment that they belong.
            Usage: Usage: <IP>:<port>/exec-registry/runs/byuser?username=<username>
        """
        # get query params from request
        try:
            username = request.data["username"]
            runs = Run.objects.filter(username=username)
            serializer = RunSerializer(runs, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)
        except (EmptyResultSet, ObjectDoesNotExist, KeyError, Exception):
            msg = "Something went wrong. Please check that you have provided a user"
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class UploadView(viewsets.ModelViewSet):
    """
        View for Upload model providing API calls for Upload model creation, update or delete as well as retrieving
        DB entries in uploads table.
    """
    permission_classes = (IsAuthenticated,)
    lookup_field = "id"
    serializer_class = UploadSerializer
    queryset = Upload.objects.all()

    def create(self, request, *args, **kwargs):
        """
            API endpoint to store a new Upload in the DB and upload a new file in the DARE platform's shared file system
            Call:

            <IP>:<port>/exec-registry/uploads/                 POST request
            Necessary field in the request: the file to be uploaded
        """
        # get data from request
        try:
            username = request.data["username"]
            filename = request.data["filename"]
            file = request.data["files"][filename]
        except (KeyError, Exception):
            msg = "User or file were not provided!"
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        dataset_name = request.data["dataset_name"] if "dataset_name" in request.data.keys() else utils.DEFAULT_DATASET
        dare_platform = request.data[
            "dare_platform"] if "dare_platform" in request.data.keys() else utils.DEFAULT_DARE_PLATFORM
        path = request.data["path"] if "path" in request.data.keys() else None
        try:
            upload_service = UploadService()
            upload = upload_service.create_upload(username, dare_platform, dataset_name, file, filename, path)
            if isinstance(upload, str):
                return Response(upload, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
            else:
                serializer = UploadSerializer(upload, many=False, context={'request': request})
                return Response(serializer.data, status=status.HTTP_200_OK)
        except (IntegrityError, OSError):
            msg = "Error while persisting Upload for token {} and dataset name {}".format(username, dataset_name)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @action(methods=['post'], detail=False, url_name="update-upload", url_path="update-upload")
    def update_upload(self, request, *args, **kwargs):
        """
            API endpoint to update an existing Upload. The user can either rename the folder where some uploaded files
            are stored or a specific file

            Usage: <IP>:<port>/exec-registry/uploads/update-upload/                 POST request

            Mandatory field in the request: the current name of the file. If the file is stored in a directory, the
            request should include the current name of the folder. The update action can refer to the folder (if exists)
            or the aforementioned file. Ih this case the relevant name(s) should be available in the request
            (new folder name and / or new file name)
        """
        # get data from request
        try:
            username = request.data["username"]
            old_filename = request.data["filename"]
        except (KeyError, Exception) as e:
            msg = "Username or filename are missing!"
            logger.error("{} {}".format(msg, e))
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        old_path = request.data["folder"] if "folder" in request.data.keys() else None
        dare_platform = request.data[
            "dare_platform"] if "dare_platform" in request.data.keys() else utils.DEFAULT_DARE_PLATFORM
        new_filename = request.data["new_filename"] if "new_filename" in request.data.keys() else None
        new_folder_name = request.data["new_folder"] if "new_folder" in request.data.keys() else None
        new_file = request.data["files"][new_filename]
        # check request
        if not username or not old_filename:
            msg = "User and/or file name are not provided"
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        try:
            # update upload in DB and in shared file system
            upload_service = UploadService()
            upload = upload_service.update_upload(username, dare_platform, old_filename, old_path,
                                                  new_filename, new_folder_name, new_file)
            if not isinstance(upload, str):
                serializer = UploadSerializer(upload, many=False, context={'request': request})
                return Response(serializer.data, status=status.HTTP_200_OK)
            else:
                return Response(upload, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        except (EmptyResultSet, ObjectDoesNotExist, MultipleObjectsReturned):
            msg = "Error while updating file {} in folder {} for user {}".format(old_filename, old_path, username)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @action(methods=['delete'], detail=False, url_name="delete-upload", url_path="delete-upload")
    def destroy_upload(self, request, *args, **kwargs):
        # get data from request
        try:
            username = request.data["username"]
            filename = request.data["filename"]
        except(KeyError, Exception) as e:
            msg = "Username or filename are missing"
            logger.error("{} {}".format(msg, e))
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        folder = request.data["folder"] if "folder" in request.data.keys() else None
        dare_platform = request.data[
            "dare_platform"] if "dare_platform" in request.data.keys() else utils.DEFAULT_DARE_PLATFORM

        try:
            # delete upload
            upload_service = UploadService()
            msg = upload_service.delete_upload(username, folder, filename, dare_platform)
            return Response(msg, status=status.HTTP_200_OK)
        except (EmptyResultSet, ObjectDoesNotExist, MultipleObjectsReturned, OSError):
            msg = "Error while deleting Run with name {}".format(request.data["run_name"])
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @action(methods=['get'], detail=False, url_name="by_user", url_path="byuser")
    def get_by_user(self, request, *args, **kwargs):
        """
            API endpoint that retrieves the uploads of a user
            Usage: <IP>:<port>/exec-registry/uploads/byuser?access_token=<token>
        """
        # get data from request
        try:
            username = request.data["username"]
        except(KeyError, Exception) as e:
            msg = "User is not provided"
            logger.error("{} {}".format(msg, e))
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        dare_platform = request.query_params[
            "dare_platform"] if "dare_platform" in request.query_params.keys() else utils.DEFAULT_DARE_PLATFORM

        try:
            # list folders in uploads (distinct)
            upload_service = UploadService()
            uploads = upload_service.get_upload_folders(username, dare_platform)
            serializer = UploadSerializer(uploads, many=True, context={'request': request})
            return Response(serializer.data, status=status.HTTP_200_OK)
        except (EmptyResultSet, ObjectDoesNotExist, MultipleObjectsReturned):
            msg = "Uploads were not found for user {}".format(username)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @action(methods=['get'], detail=False, url_name="by_folder", url_path="byfolder")
    def get_by_folder(self, request):
        """
            API endpoint to retrieve the files of a user in a specific folder
            usage: <IP>:<port>/exec-registry/uploads/byfolder?username=<username>&folder=<folder>
        """
        try:
            username = request.data["username"]
            folder = request.query_params["folder"]
        except(KeyError, Exception) as e:
            msg = "Please provide a user and a folder"
            logger.error("{} {}".format(msg, e))
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        dare_platform = request.query_params["dare_platform"] if \
            "dare_platform" in request.query_params.keys() else utils.DEFAULT_DARE_PLATFORM
        utils.check_create_dirs(utils.d4p_sfs['mountpath'], [username, utils.UPLOADS])
        base_path = join(utils.d4p_sfs['mountpath'], username, utils.UPLOADS)

        # list files in folder
        upload_service = UploadService()
        upload_path = join(base_path, folder)
        uploads = upload_service.get_uploads(upload_path, username, dare_platform)
        serializer = UploadSerializer(uploads, many=False, context={'request': request})
        return Response(serializer.data, status=status.HTTP_200_OK)


class DownloadView(viewsets.GenericViewSet):
    """
        View to download files or retrieve full paths in the DARE platform
    """
    permission_classes = (IsAuthenticated,)

    @action(methods=["get"], detail=False, url_name="download", url_path="download")
    def download(self, request):
        """
            API to download a file from the DARE platform. It is assumed that the file is in an execution directory.
            usage: <IP>:<port>/exec-registry/downloads/download?username=<username>&experiment_name=<experiment_name>&
                                                                run_name=<run_name>&filename=<filename>
        """

        # get data from request
        try:
            username = request.data["username"]
            experiment_name = request.query_params["experiment_name"]
            run_name = request.query_params["run_name"]
            filename = request.query_params["filename"]
        except(KeyError, Exception) as e:
            msg = "User, experiment or run are not provided"
            logger.error("{} {}".format(msg, e))
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        dare_platform = request.query_params[
            "dare_platform"] if "dare_platform" in request.query_params.keys() else utils.DEFAULT_DARE_PLATFORM

        experiment_group_name = request.query_params[
            "experiment_group_name"] if "experiment_group_name" in \
                                        request.query_params.keys() else utils.DEFAULT_EXPERIMENT_GROUP

        download_service = DownloadService()
        path = download_service.get_download_path(username, dare_platform, experiment_group_name, experiment_name,
                                                  run_name, filename)
        if exists(path):
            response = HttpResponse(open(path, 'r').read())
            response['Content-Type'] = 'text/plain'
            response['Content-Disposition'] = 'attachment; filename=' + filename
            return response
        else:
            msg = "Given file does not exist"
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @action(methods=["get"], detail=False, url_name="full_path", url_path="fullpath")
    def get_full_path(self, request):
        """
            API call to retrieve a path to a file in the platform
            usage: <IP>:<port>/exec-registry/downloads/fullpath?username=<username>&kind=<kind>
        """
        username = request.data["username"]
        kind = request.query_params["kind"]
        download_service = DownloadService()
        names = []
        if kind == "experiments":
            experiment_group_name = request.query_params[
                "experiment_group_name"] if "experiment_group_name" in \
                                            request.query_params.keys() else None
            group_to_be_used = True if "experiment_group_name" in request.query_params.keys() else False
            experiment_name = request.query_params[
                "experiment_name"] if "experiment_name" in request.query_params.keys() else None
            run_name = request.query_params["run_name"] if "run_name" in request.query_params.keys() else None
            names = [experiment_group_name, experiment_name, run_name] if group_to_be_used else \
                [experiment_name, run_name]
        elif kind == "uploads":
            folder = request.query_params["folder"] if "folder" in request.query_params.keys() else None
            filename = request.query_params["filename"] if "filename" in request.query_params.keys() else None
            names = []
            if folder:
                names.append(folder)
            if filename:
                names.append(filename)
        path = download_service.get_full_path(kind, username, names)
        return JsonResponse({'path': path})


class LoginView(viewsets.GenericViewSet):
    model = User
    permission_classes = (AllowAny,)
    serializer_class = UserSerializer

    @action(methods=["POST"], detail=False, url_name="login", url_path="login")
    def login(self, request, *args, **kwargs):
        username = request.data["username"]
        password = request.data["password"]
        token = request.data["access_token"]
        email = request.data["email"]
        given_name = request.data["given_name"]
        family_name = request.data["family_name"]
        try:
            u = User.objects.get(username=username)
        except (IntegrityError, Exception):
            print("User {} not found! Creating user".format(username))
            try:
                u = User.objects.create_superuser(username=username, password=password, email=email,
                                                  first_name=given_name, last_name=family_name)
                u.save()
                print("User {} is saved".format(username))
            except (IntegrityError, Exception) as e:
                msg = "An error occurred while saving user {}".format(e)
                return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        return JsonResponse(json.dumps({"access_token": token}), safe=False, status=status.HTTP_200_OK)
