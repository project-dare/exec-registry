import logging
import os
import shutil
from datetime import datetime
from os.path import join, exists
from werkzeug.utils import secure_filename
from django.core.exceptions import EmptyResultSet, ObjectDoesNotExist, MultipleObjectsReturned

from registry import utils
from registry.models import ExperimentGroup, Experiment, Run, Upload

logger = logging.getLogger(__name__)


class ExperimentGroupService:
    """
        Class containing backend methods for the ExperimentGroup Django model. Implements methods for DB entries
        manipulation (e.g. create, update, delete, retrieve) and additonal actions on the folders of the Shared File
        System in the DARE platform
    """

    def create_experiment_group(self, experiment_group_name, username, dare_platform, create_dirs=True):
        """
            Method used to create a new experiment group. Autogenerates a name if it's missing. Creates an
            ExperimentGroup object and stores it in the DB and creates a new directory in the shared file system
            with the respective name.

            Args
                experiment_group_name (str): the name of the experiment group to be created
                username (str): the username of the user who sent the request
                dare_platform (int): the number of the specific DARE platform
                create_dirs (bool): True/False if directories need to be created

            Returns
                ExperimentGroup: the ExperimentGroup object saved in the DB
        """
        if not experiment_group_name:
            logger.debug("Generating new group name")
            experiment_group_name = self.__auto_create_experiment_name(username, dare_platform)
            logger.debug("Auto group name: {}".format(experiment_group_name))
        if self.__check_experiment_group_exists(experiment_group_name, username, dare_platform):
            return "Experiment group with name {} is already registered for user {}".format(
                experiment_group_name, username)
        else:
            experiment_group = ExperimentGroup(name=experiment_group_name, username=username,
                                               dare_platform=dare_platform)
            logger.debug("Saving experiment group {}".format(experiment_group.__str__()))
            experiment_group.save()
            if create_dirs:
                utils.check_create_dirs(base_path=utils.d4p_sfs['mountpath'],
                                        dir_names=[username, utils.EXPERIMENTS, experiment_group_name])
            return experiment_group

    def update_experiment_group(self, new_experiment_group_name, dare_platform, username, experiment_group_name):
        """
            Updates an experiment group in the database and the shared file system of the DARE platform.

            Args
                new_experiment_group_name (str): the new experiment group name to update the folder and the DB entry
                dare_platform (int): the number of the specific DARE platform
                username (str): the username of the user who sent the request
                experiment_group_name (str): the old experiment group name so as to find the target DB entry

            Returns
                ExperimentGroup: an object with the updated experiment group
        """
        experiment_group = self.get_experiment_group(experiment_group_name, dare_platform, username)
        utils.check_create_dirs(utils.d4p_sfs['mountpath'], [username, utils.EXPERIMENTS])
        base_path = join(utils.d4p_sfs['mountpath'], username, utils.EXPERIMENTS)
        experiment_group_dir = join(base_path, experiment_group_name)
        if experiment_group and new_experiment_group_name:
            if self.__check_experiment_group_exists(new_experiment_group_name, username,
                                                    dare_platform):
                return "Experiment group with name {} is already registered for user {}".format(
                    new_experiment_group_name, username)
            new_experiment_group_dir = join(base_path, new_experiment_group_name)
            self.__rename_experiment_group_folder(experiment_group_dir, new_experiment_group_dir)
            experiment_group.name = new_experiment_group_name
            experiment_group.save()
            return experiment_group
        else:
            return "Experiment group with name {} for user {} was not found".format(experiment_group_name,
                                                                                    username)

    def delete_experiment_group(self, experiment_group_name, username, dare_platform):
        """
            Deletes a target experiment group from the DB and removes the folder from the Shared File System of
            the DARE platform.

            Args
                experiment_group_name (str): the target experiment group's name
                username (str): the username of the user who sent the request
                dare_platform (int): the number of the specific DARE platform

            Returns
                bool: True/False if the request was successful or not
        """
        experiment_group = self.get_experiment_group(experiment_group_name, dare_platform, username)
        base_path = join(utils.d4p_sfs['mountpath'], username, utils.EXPERIMENTS)
        if experiment_group:
            experiment_group.delete()
            experiment_group_dir = join(base_path, experiment_group.name)
            if exists(experiment_group_dir):
                os.rmdir(experiment_group_dir)
            return True
        return False

    @staticmethod
    def get_experiment_group(experiment_group_name, dare_platform, username):
        """
            Method to find a target ExperimentGroup

            Args
                experiment_group_name (str): the target experiment group's name
                username (str): the username of the user who sent the request
                dare_platform (int): the number of the specific DARE platform

            Returns
                ExperimentGroup: the ExperimentGroup object found in the DB
        """
        return ExperimentGroup.objects.get(name__exact=experiment_group_name,
                                           username__exact=username,
                                           dare_platform__exact=dare_platform)

    @staticmethod
    def get_experiment_groups(username, dare_platform):
        """
            Method to retrieve ExperimentGroup object based on the username and dare_platform.

            Args
               username (str): the username of the user who sent the request
                dare_platform (int): the number of the specific DARE platform

            Returns
                list: the ExperimentGroup list found in the DB
        """
        return ExperimentGroup.objects.filter(username=username, dare_platform=dare_platform)

    @staticmethod
    def __rename_experiment_group_folder(experiment_group_dir, new_experiment_group_dir):
        """
            Function to update the experiment group's folder name in the Shared File System of the DARE platform

            Args
                experiment_group_dir (str): the old folder name
                new_experiment_group_dir (str): the new folder name
        """
        os.rename(experiment_group_dir, new_experiment_group_dir)

    @staticmethod
    def __check_experiment_group_exists(experiment_group_name, username, dare_platform):
        """
            Checks if an experiment group exists based on its name, the username and the dare_platform identifier

            Args
                experiment_group_name (str): the target experiment group's name
                username (str): the username of the user who sent the request
                dare_platform (int): the number of the specific DARE platform

            Returns
                bool: True/False if experiment group is found
        """
        user_experiment_group = ExperimentGroup.objects.filter(name=experiment_group_name, username=username,
                                                               dare_platform=dare_platform).count()
        return False if user_experiment_group == 0 else True

    def __auto_create_experiment_name(self, username, dare_platform):
        """
            Retrieves all the experiment groups of the user and keeps those with auto-generated name.
            Updates the counter accordingly to generate a new ExperimentGroup name.

            Args
                username (str): the username of the user who sent the request
                dare_platform (int): the number of the specific DARE platform

            Returns
                str: the auto-generated experiment group name
        """
        try:
            user_experiment_groups = self.get_experiment_groups(username, dare_platform)
        except (EmptyResultSet, ObjectDoesNotExist, MultipleObjectsReturned, Exception):
            user_experiment_groups = None
            pass
        count = 1
        if user_experiment_groups:
            for exp in user_experiment_groups:
                if exp.name.startswith("Group"):
                    count += 1
        return "Group" + str(count)


class ExperimentService:
    """
    Class containing backend methods for the Experiment model (create, update, delete and retrieve) as well as
    additional methods to handle experiments in the Shared File System of the DARE platform
    """

    def create_experiment(self, experiment_group_name, username, dare_platform, group_to_be_used, experiment_name):
        """
            Creates a new experiment in the DB and shared file system of the DARE platform

            Args
                experiment_group_name (str): the name of the relevant ExperimentGroup. If not provided, the default
                group name is used and no directory is generated in the path in the SFS
                username (str): the username of the user performed the request
                dare_platform (int): the target DARE platform (if not provided a default platform is used)
                group_to_be_used (bool): True/False based on whether the experiment group name is provided
                experiment_name (str): the name of the experiment to be generated. If not provided, the name is
                auto-generated.

            Returns
                Experiment: the created Experiment object
        """
        experiment_group_service = ExperimentGroupService()
        try:
            experiment_group = experiment_group_service.get_experiment_group(experiment_group_name, dare_platform,
                                                                             username)
        except (EmptyResultSet, ObjectDoesNotExist, MultipleObjectsReturned, Exception):
            experiment_group = None
            pass
        utils.check_create_dirs(base_path=utils.d4p_sfs['mountpath'], dir_names=[username, utils.EXPERIMENTS])
        base_path = join(utils.d4p_sfs['mountpath'], username, utils.EXPERIMENTS)
        if not experiment_group:
            # create experiment group if it does not exist
            experiment_group = experiment_group_service.create_experiment_group(experiment_group_name, username,
                                                                                dare_platform,
                                                                                create_dirs=group_to_be_used)
        # generate experiment name
        if not experiment_name:
            experiment_name = self.__auto_create_experiment_name(experiment_group_name, username, dare_platform)

        # check if experiment already exists
        if self.__check_experiment_exists(username, dare_platform, experiment_group_name, experiment_name):
            return "Experiment with name {} for user {} already exists".format(experiment_name, username)

        # save experiment in DB
        experiment = Experiment(name=experiment_name, username=username, group_to_be_used=group_to_be_used,
                                experiment_group=experiment_group)
        experiment.save()

        # create experiment directory
        utils.check_create_dirs(base_path=base_path, dir_names=[experiment_name]) \
            if not group_to_be_used else utils.check_create_dirs(base_path=base_path, dir_names=[experiment_group_name,
                                                                                                 experiment_name])
        return experiment

    def update_experiment(self, username, dare_platform, experiment_group_name, experiment_name,
                          new_experiment_group_name, new_experiment_name):
        """
            Updates an existing experiment. Experiment group and experiment names can be updated (both in the DB and
            the Shared File System of the DARE platform).

            Args
                username (str): the username of the user performed the request
                dare_platform (int): the target DARE platform (if not provided a default platform is used)
                experiment_group_name (str): if applied, the name of the experiment group where the experiment belongs
                (optional)
                experiment_name (str): the name of the target experiment
                new_experiment_group_name (str): the new name for the experiment group (optional)
                new_experiment_name (str): the new name for the experiment (optional)

            Returns
                Experiment: the updated Experiment object
        """
        # get the experiment to be updated
        try:
            experiment = self.get_experiment(username, dare_platform, experiment_group_name, experiment_name)
        except (EmptyResultSet, ObjectDoesNotExist, MultipleObjectsReturned, Exception):
            # error if experiment does not exist
            return "Experiment with name {} for user {} was not found!".format(experiment_name, username)

        # create the old and new directory paths
        utils.check_create_dirs(utils.d4p_sfs['mountpath'], [username, utils.EXPERIMENTS])
        base_path = join(utils.d4p_sfs['mountpath'], username, utils.EXPERIMENTS)
        old_experiment_dir = self.__get_experiment_dir(base_path, experiment.group_to_be_used, experiment_group_name)
        new_experiment_dir = self.__get_experiment_group_dir(base_path, experiment.group_to_be_used,
                                                             experiment_group_name,
                                                             new_experiment_group_name)

        # check if experiment group should be updated (move experiment under different experiment group)
        if new_experiment_group_name:
            experiment_dir = join(old_experiment_dir, experiment_name)
            experiment_group = self.__update_experiment_group(username, dare_platform, new_experiment_group_name)
            experiment.experiment_group = experiment_group
            experiment.group_to_be_used = True
            experiment.save()
            shutil.move(experiment_dir, new_experiment_dir, copy_function=shutil.copytree)
            old_experiment_dir = new_experiment_dir

        # check if experiment name should be updated
        if new_experiment_name:
            # check if the new name given by the user already exists
            if self.__check_experiment_exists(username, dare_platform, experiment.experiment_group.name,
                                              new_experiment_name):
                return "Experiment with name {} already exists for user {}".format(new_experiment_name, username)
            # update the experiment
            old_experiment_dir = join(old_experiment_dir, experiment_name)
            new_experiment_dir = join(new_experiment_dir, new_experiment_name)
            experiment.name = new_experiment_name
            experiment.save()
            os.rename(old_experiment_dir, new_experiment_dir)
        return experiment

    def delete_experiment(self, username, dare_platform, experiment_group_name, experiment_name):
        """
            Deletes a target experiment from the DB and the Shared File System. Experiment Group is also deleted if no
            experiments are left in the directory.

            Args
                username (str): the username of the user performed the request
                dare_platform (int): the target DARE platform (if not provided a default platform is used)
                experiment_group_name (str): if applied, the name of the experiment group where the experiment belongs
                (optional)
                experiment_name (str): the name of the target experiment

            Returns
                bool: if deletion was successful
        """
        try:
            experiment = self.get_experiment(username, dare_platform, experiment_group_name, experiment_name)
            base_path = join(utils.d4p_sfs['mountpath'], username, utils.EXPERIMENTS)
            experiment_dir = join(base_path, experiment.name) if not experiment.group_to_be_used \
                else join(base_path, experiment.experiment_group.name, experiment.name)
            os.rmdir(experiment_dir)
            experiment.delete()
            if experiment.group_to_be_used:
                experiment_group_dir = join(base_path, experiment_group_name)
                files = os.listdir(experiment_group_dir)
                if not files:
                    os.rmdir(experiment_group_dir)
                    experiment_group = ExperimentGroup.objects.get(username=username, dare_platform=dare_platform,
                                                                   name=experiment_group_name)
                    experiment_group.delete()
            return True
        except (EmptyResultSet, ObjectDoesNotExist, MultipleObjectsReturned, OSError):
            return False

    @staticmethod
    def get_experiment(username, dare_platform, experiment_group_name, experiment_name):
        """
            Retrieves a single experiment from the DB based on the username, experiment group and experiment name.

            Args
                username (str): the username of the user performed the request
                dare_platform (int): the target DARE platform (if not provided a default platform is used)
                experiment_group_name (str): if applied, the name of the experiment group where the experiment belongs
                (optional)
                experiment_name (str): the name of the target experiment

            Returns
                Experiment: the retrieved experiment from the DB
        """
        experiment_group_service = ExperimentGroupService()
        experiment_group = experiment_group_service.get_experiment_group(experiment_group_name, dare_platform,
                                                                         username)
        return Experiment.objects.get(username=username, experiment_group=experiment_group,
                                      name=experiment_name)

    @staticmethod
    def get_experiments(experiment_group_name, username, dare_platform):
        """
            Retrieves a list of experiments from the DB based on the experiment group and the user

            Args
                username (str): the username of the user performed the request
                dare_platform (int): the target DARE platform (optional - if not provided a default platform is used)
                experiment_group_name (str): if applied, the name of the experiment group where the experiment belongs
                (optional)

            Returns
                list: Experiment objects list retrieved from the DB
        """
        experiment_group_service = ExperimentGroupService()
        experiment_group = experiment_group_service.get_experiment_group(experiment_group_name, dare_platform,
                                                                         username)
        return Experiment.objects.filter(username=username, experiment_group=experiment_group)

    @staticmethod
    def __update_experiment_group(username, dare_platform, experiment_group_name):
        """
            Updates an existing experiment group prior updating the target experiment

            Args
                username (str): the username of the user performed the request
                dare_platform (int): the target DARE platform (optional - if not provided a default platform is used)
                experiment_group_name (str): if applied, the name of the experiment group where the experiment belongs
                (optional)

            Returns
                ExperimentGroup: the updated ExperimentGroup object
        """
        try:
            experiment_group = ExperimentGroup.objects.get(username=username, dare_platform=dare_platform,
                                                           name=experiment_group_name)
        except (EmptyResultSet, ObjectDoesNotExist, MultipleObjectsReturned, Exception):
            experiment_group = ExperimentGroup(name=experiment_group_name, username=username,
                                               dare_platform=dare_platform)
            experiment_group.save()
        return experiment_group

    def __auto_create_experiment_name(self, experiment_group_name, username, dare_platform):
        """
            Auto-generates an experiment name when it is not provided upon experiment creation

            Args
                username (str): the username of the user performed the request
                dare_platform (int): the target DARE platform (optional - if not provided a default platform is used)
                experiment_group_name (str): if applied, the name of the experiment group where the experiment belongs
                (optional)

            Returns
                str: the auto-generated experiment name
        """
        try:
            user_experiments = self.get_experiments(experiment_group_name, username, dare_platform)
        except (EmptyResultSet, ObjectDoesNotExist, MultipleObjectsReturned, Exception):
            user_experiments = None
            pass
        count = 1
        if user_experiments:
            if user_experiments:
                for exp in user_experiments:
                    if exp.name.startswith("Experiment"):
                        count += 1
        return "Experiment" + str(count)

    @staticmethod
    def __check_experiment_exists(username, dare_platform, experiment_group_name, experiment_name):
        """
            Checks if a target experiment (based on the user, the experiment group and the experiment name) already
            exists in the DB

            Args
                username (str): the username of the user performed the request
                dare_platform (int): the target DARE platform (optional - if not provided a default platform is used)
                experiment_group_name (str): if applied, the name of the experiment group where the experiment belongs
                (optional)
                experiment_name (str): the name of the target experiment

            Returns
                bool: True/False based on whether an experiment with these args is found in the DB
        """
        experiment_group_service = ExperimentGroupService()
        experiment_group = experiment_group_service.get_experiment_group(experiment_group_name, dare_platform,
                                                                         username)
        count = Experiment.objects.filter(username=username, experiment_group=experiment_group,
                                          name=experiment_name).count()
        return True if count != 0 else False

    @staticmethod
    def __get_experiment_group_dir(base_path, group_to_be_used, experiment_group_name, new_experiment_group_name):
        """
            Based on whether experiment group is used in the SFS by the user, creates the path to the target experiment
            group with the updated name. If a new experiment group name is provided, it is used in the path, otherwise
            checks if experiment group should be used in the path and adds the old name in the path.

            Args
                base_path (str): the user experiments directory
                group_to_be_used (bool): True/False based on whether the experiment group will be used in the path
                experiment_group_name (str): the old experiment group name
                new_experiment_group_name (str): the new experiment group name
        """
        return join(base_path, new_experiment_group_name) if new_experiment_group_name else join(
            base_path, experiment_group_name) if group_to_be_used else join(base_path)

    @staticmethod
    def __get_experiment_dir(base_path, group_to_be_used, experiment_group_name, experiment_name=None):
        """
            Creates the path towards the target experiment

            Args
                base_path (str): the user experiments directory
                group_to_be_used (bool): True/False based on whether the experiment group will be used in the path
                experiment_group_name (str): the old experiment group name
                experiment_name (str): the name of the experiment
        """
        if not experiment_name:
            return join(base_path, experiment_group_name) if group_to_be_used else base_path
        else:
            return join(base_path, experiment_group_name, experiment_name) if group_to_be_used else join(
                base_path, experiment_name)


class RunService:
    """
        Class providing backend methods for Run model. Implements basic actions on the DB entries, such as creating,
        updating, deleting and retrieving entries. Additionally, provides functionality on the folders/files in the
        Shared File System associated with exectution directories.
    """

    def create_run(self, username, kind, experiment, run_name=None):
        """
            Creates a new run in the database and a new execution folder in the Shared File System.

            Args
                username (str): the username of the user performing the request
                kind (str): can take only two values, i.e. trial or official
                experiment (str): the associated Experiment object
                run_name (str): the name of the new run (optional)

            Returns
                Run: the created Run object
        """
        if not run_name:
            # auto-generates a new run name if it is not provided by the user
            run_name = self.__auto_create_run_name(username, experiment)
        date = datetime.now()
        kind = self.__find_kind_code(kind=kind)
        run_dir = self.__create_run_dir(username=username, kind=kind, date=date, run_name=run_name)
        self.__create_dirs(username, experiment.group_to_be_used, experiment, run_dir)
        run = Run(username=username, experiment=experiment, date=date, name=run_name, kind=kind, run_dir=run_dir)
        run.save()
        return run

    def update_run(self, username, dare_platform, experiment_group_name, experiment_name, run_name,
                   new_run_name, new_experiment_name, new_experiment_group_name):
        """
            Updates an existing run in the DB and the DARE platform's Shared File System. One of the new names (run,
            experiment or experiment group) should be provided in order to perform any update action.

            Args
                username (str): the username of the user performing the request
                dare_platform (int): the target DARE platform (optional - if not provided a default platform is used)
                experiment_group_name (str): if applied, the name of the experiment group where the experiment belongs
                (optional)
                experiment_name (str): the name of the associated experiment
                run_name (str): the name of the target run
                new_run_name (str): a new name for the target run (optional)
                new_experiment_name (str): a new name for the associated experiment (optional)
                new_experiment_group_name (str): a new experiment group name (optional)
        """
        # check directories for user
        utils.check_create_dirs(utils.d4p_sfs['mountpath'], [username, utils.EXPERIMENTS])
        base_path = join(utils.d4p_sfs['mountpath'], username, utils.EXPERIMENTS)
        # find run to be updated
        run = self.get_run(username, dare_platform, experiment_group_name, experiment_name, run_name)

        if not run:
            return "Run with name {} for user {} and experiment {} was not found".format(run_name, username,
                                                                                         experiment_name)
        # check for new run directory
        if new_run_name:
            run = self.__update_run(base_path, run.experiment.group_to_be_used, experiment_group_name,
                                    experiment_name, run, new_run_name)
        if new_experiment_name:
            # check if experiment should be updated
            if not self.__check_move_to_experiment(run, new_experiment_name, username):
                return "Experiment already exists for user {}".format(username)
            path = join(base_path, experiment_group_name) if run.experiment.group_to_be_used else base_path
            run = self.__update_experiment(path, run, new_experiment_name, username)

        # check if experiment group should be updated
        if new_experiment_group_name:
            if not self.__check_move_to_experiment_group(run.experiment, new_experiment_group_name, username,
                                                         dare_platform):
                return "Experiment for user {} already exists for group {}".format(username, new_experiment_group_name)

            self.__update_experiment_group(base_path, run.experiment.group_to_be_used, experiment_group_name,
                                           new_experiment_group_name, dare_platform, username, run.experiment)
        return run

    def delete_run(self, username, dare_platform, experiment_group_name, experiment_name, run_name):
        """
            Deletes a target run from the DB and the Shared File System. Cleans the possible empty folders (experiment
            or experiment group) as well.

            Args
                username (str): the username of the user performing the request
                dare_platform (int): the target DARE platform (optional - if not provided a default platform is used)
                experiment_group_name (str): if applied, the name of the experiment group where the experiment belongs
                (optional)
                experiment_name (str): the name of the associated experiment
                run_name (str): the name of the target run
        """
        base_path = join(utils.d4p_sfs['mountpath'], username, utils.EXPERIMENTS)
        run = self.get_run(username, dare_platform, experiment_group_name, experiment_name, run_name)
        if run.experiment.group_to_be_used:
            experiment_group_dir = join(base_path, experiment_group_name)
            experiment_dir = join(experiment_group_dir, experiment_name)
            run_dir = join(experiment_dir, run.run_dir)
        else:
            experiment_group_dir = base_path
            experiment_dir = join(experiment_group_dir, experiment_name)
            run_dir = join(base_path, experiment_name, run.run_dir)
        # delete run
        os.rmdir(run_dir)
        run.delete()
        self.__delete_empty_dirs(username, dare_platform, experiment_group_name, experiment_name, experiment_dir,
                                 experiment_group_dir)

    @staticmethod
    def get_run(username, dare_platform, experiment_group_name, experiment_name, run_name):
        """
            Retrieves a single run from the DB based on the user, the run name the experiment and the experiment group.

            Args
                username (str): the username of the user performing the request
                dare_platform (int): the target DARE platform (optional - if not provided a default platform is used)
                experiment_group_name (str): if applied, the name of the experiment group where the experiment belongs
                (optional)
                experiment_name (str): the name of the associated experiment
                run_name (str): the name of the target run

            Returns
                Run: the retrieved Run object
        """
        try:
            experiment_group = ExperimentGroup.objects.get(username=username, dare_platform=dare_platform,
                                                           name=experiment_group_name)
            experiment = Experiment.objects.get(username=username, name=experiment_name,
                                                experiment_group=experiment_group)
            run = Run.objects.get(username=username, experiment=experiment, name=run_name)
            return run
        except (EmptyResultSet, ObjectDoesNotExist, MultipleObjectsReturned, Exception) as e:
            print(e)
            return None

    @staticmethod
    def get_runs(username, experiment):
        """
            Retrieves a Run list from the DB for the specified user

            Args
                username (str): the username of the target user
                experiment (Experiment): the associated Experiment object

            Returns
                list: a list of relevant Run objects
        """
        return Run.objects.filter(username=username, experiment=experiment)

    @staticmethod
    def get_runs_by_kind_and_experiment(username, experiment, kind):
        """
            Retrieves a list of Run objects based on an experiment, the user and the run kind (trial or official).

            Args
                username (str): the username of the target user
                experiment (Experiment): the associated Experiment object
                kind (str): can take only two values, i.e. trial or official

            Returns
                list: a list of relevant Run objects
        """
        return Run.objects.filter(username=username, experiment=experiment, kind=kind)

    def get_runs_from_scratch(self, username, dare_platform, experiment_group_name, experiment_name, kind):
        """
            Retrieves experiment and runs from the DB

            Args
                username (str): the username of the target user
                dare_platform (int): the target DARE platform (optional - if not provided a default platform is used)
                experiment_group_name (str): if applied, the name of the experiment group where the experiment belongs
                (optional)
                experiment_name (str): the name of the associated experiment
        """
        if kind:
            kind = self.__find_kind_code(kind)
        experiment_service = ExperimentService()
        experiment = experiment_service.get_experiment(username, dare_platform, experiment_group_name,
                                                       experiment_name)
        return Run.objects.filter(username=username, experiment=experiment) if not kind else \
            Run.objects.filter(username=username, experiment=experiment, kind=kind)

    @staticmethod
    def __find_kind_code(kind):
        """
            Based on the word provided by the user (trial or official), the method searches the choice tuples in the
            Run model to find the respective code so as to save it in the DB

            Args
                kind (str): trial or official

            Returns
                str: the respective code (two letters)
        """
        choices = Run.KIND_CHOICES
        for choice_tuple in choices:
            if choice_tuple[1] == kind.lower():
                return choice_tuple[0]

    def __auto_create_run_name(self, username, experiment):
        """
            Auto-generates a name for a specific Run. It is based on previous auto-generated names under a specific
            experiment and for a specific user.

            Args
                username (str): the username of the user performing the request
                experiment (Experiment): the associated Experiment object

            Returns
                str: the auto-generated run name
        """
        try:
            user_runs = self.get_runs(username, experiment)
        except (EmptyResultSet, ObjectDoesNotExist, MultipleObjectsReturned, Exception):
            user_runs = None
            pass
        count = 1
        if user_runs:
            for run in user_runs:
                if run.name.startswith("Run"):
                    count += 1
        return "Run" + str(count)

    @staticmethod
    def __create_run_dir(username, kind, date, run_name):
        """
            Creates the name of an execution directory by concatenating the username, the run kind, the datetime and
            the run name

            Args
                username (str): the username of the user performing the request
                kind (str): based on the given kind (trial or official) adds the relevant code
                date(datetime): timestamp of the run creation
                run_name (str): the name of the execution

            Returns
                str: the name of the new directory in the Shared File System of the DARE platform
        """
        run_dir = username + "_" + kind + "_" + date.strftime('%Y%m%d%H%M%S') + "_" + run_name
        return run_dir

    @staticmethod
    def __create_dirs(username, group_to_be_used, experiment, run_dir):
        """
            Creates the necessary directories for a run/experiment pair for a specific user

            Args
               username (str): the username of the user performing the request
               group_to_be_used (bool): True/False --> whether experiment group will be used in the path
               experiment(Experiment): the associated Experiment object
               run_dir (str): the name of the run directory
        """
        utils.check_create_dirs(utils.d4p_sfs['mountpath'], [username, utils.EXPERIMENTS])
        base_path = join(utils.d4p_sfs['mountpath'], username, utils.EXPERIMENTS)
        utils.check_create_dirs(base_path=base_path, dir_names=[experiment.name, run_dir]) \
            if not group_to_be_used else \
            utils.check_create_dirs(base_path=base_path,
                                    dir_names=[experiment.experiment_group.name,
                                               experiment.name, run_dir])

    @staticmethod
    def __check_move_to_experiment_group(experiment, new_experiment_group_name, username, dare_platform):
        """
            If experiment group is requested to be updated, check if an experiment already exists under the specific
            experiment group.

            Args
                experiment (Experiment): the associated Experiment object
                new_experiment_group_name (str): the new name of the experiment group
                username (str): the username of the user performing the request
                dare_platform (int): the target DARE platform (optional - if not provided a default platform is used)

            Returns
                bool: True/False whether the experiment can be moved under a different experiment group
        """
        try:
            experiment_group = ExperimentGroup.objects.get(name=new_experiment_group_name, username=username,
                                                           dare_platform=dare_platform)
            if experiment_group.experiments:
                for experiment_to_be_checked in experiment_group.experiments:
                    if experiment_to_be_checked.name == experiment.name:
                        return False
            return True
        except (EmptyResultSet, ObjectDoesNotExist, MultipleObjectsReturned, Exception):
            return True

    @staticmethod
    def __check_move_to_experiment(run, new_experiment_name, username):
        """
            After having updated the run (if requested) and new_experiment_name exists, check if experiment can be
            updated based on the run's name.

            Args
                run (Run): the target Run object
                new_experiment_name (str): the new name of the experiment
                username (str): the username of the user performing the request

            Returns
                bool: True/False based on whether the run can be moved under a different experiment
        """
        try:
            experiment = Experiment.objects.get(name=new_experiment_name, username=username,
                                                experiment_group=run.experiment.experiment_group)
            if experiment.runs:
                for run_to_check in experiment.runs:
                    if run_to_check.name == run.name:
                        return False
            return True
        except (EmptyResultSet, ObjectDoesNotExist, MultipleObjectsReturned, Exception):
            return True

    @staticmethod
    def __check_run_exists(run, new_run_name, username):
        """
            Searches the DB with the new run name to find whether the specified run name already exists under the same
            experiment

            Args
                run (Run): the target Run object
                new_run_name (str): the new name of the run
        """
        experiment = run.experiment
        try:
            user_run = Run.objects.get(name=new_run_name, username=username, experiment=experiment)
            return True if user_run and user_run.run_id != run.run_id else False
        except (EmptyResultSet, ObjectDoesNotExist, MultipleObjectsReturned, Exception):
            return False

    @staticmethod
    def __update_experiment_group(base_path, group_to_be_used, experiment_group_name, new_experiment_group_name,
                                  dare_platform, username, experiment):
        """
            Updates the relevant experiment group - renames it. Experiment object is updated with the new experiment
            group and saved in the DB

            Args
                base_path (str): the experiments directory of the user
                group_to_be_used (bool): True/False whether experiment group is in the path
                experiment_group_name (str): the old experiment group name
                new_experiment_group_name (str): the new experiment group name
                dare_platform (int): the target DARE platform (optional - if not provided a default platform is used)
                username (str): the username of the user performing the request
                experiment (Experiment): the associated Experiment object
        """
        utils.check_create_dirs(base_path, [new_experiment_group_name])
        old_path = join(base_path, experiment_group_name) if group_to_be_used else base_path
        new_path = join(base_path, new_experiment_group_name)

        experiment_dir = join(old_path, experiment.name)

        shutil.move(experiment_dir, new_path, copy_function=shutil.copytree)
        try:
            experiment_group = ExperimentGroup.objects.get(username=username, dare_platform=dare_platform,
                                                           name=new_experiment_group_name)
        except (EmptyResultSet, ObjectDoesNotExist, MultipleObjectsReturned, Exception):
            experiment_group = ExperimentGroup(username=username, dare_platform=dare_platform,
                                               name=new_experiment_group_name)
            experiment_group.save()
        experiment.group_to_be_used = True
        experiment.experiment_group = experiment_group
        experiment.save()

    @staticmethod
    def __update_experiment(path, run, new_experiment_name, username):
        """
            Renames the experiment associated with a target run. Updates both the Shared file system and the DB. The
            target run is updated and saved in the DB

            Args
                path (str): the path to the experiment
                run (Run): the target Run object
                new_experiment_name (str): the new name of the experiment
                username (str): the username of the user performing the request

            Returns
                Run: the updated execution
        """
        new_path = join(path, new_experiment_name)
        old_path = join(path, run.experiment.name, run.run_dir)
        shutil.move(old_path, new_path, copy_function=shutil.copytree)

        try:
            experiment = Experiment.objects.get(username=username, name=new_experiment_name,
                                                experiment_group=run.experiment.experiment_group)
        except (EmptyResultSet, ObjectDoesNotExist, MultipleObjectsReturned, Exception):
            experiment = Experiment(name=new_experiment_name, username=username,
                                    experiment_group=run.experiment.experiment_group)
            experiment.save()

        run.experiment = experiment
        run.save()
        return run

    def __update_run(self, base_path, group_to_be_used, experiment_group_name, experiment_name, run, new_run_name):
        """
            Uses new_run_name to update the specified folder and the relevant DB entry

            Args
                base_path (str): the experiments directory of the user
                group_to_be_used (bool): True/False whether experiment group is in the path
                experiment_group_name (str): the name of the associated experiment group
                experiment_name (str): the name of the associated experiment
                run (Run): the Run object required by the user to be updated
                new_run_name (str): the new name of the target run

            Returns
                Run: the updated execution
        """
        # create path
        path = join(base_path, experiment_group_name) if group_to_be_used else base_path
        path = join(path, experiment_name)
        old_run_dir = run.run_dir
        new_run_dir = self.__update_new_run_dir(new_run_name, run)
        old_path = join(path, old_run_dir)
        new_path = join(path, new_run_dir)

        # update DB
        run.name = new_run_name
        run.run_dir = new_run_dir
        run.save()

        # rename directory
        os.rename(old_path, new_path)
        return run

    @staticmethod
    def __update_new_run_dir(new_run_name, run):
        """
            Updates the name of the run directory if user has requested the Run to be renamed.

            Args
                new_run_name (str): the new name of the run
                run (Run): the requested Run object

            Returns
                str: the new directory name
        """
        run_dir_parts = run.run_dir.split("_")
        run_dir_parts[3] = new_run_name
        return "_".join(run_dir_parts)

    @staticmethod
    def __delete_empty_dirs(username, dare_platform, experiment_group_name, experiment_name, experiment_dir,
                            experiment_group_dir):
        """
            Deletes experiment and experiment group directories if empty once the target run is removed.

            Args
                username (str): the username of the user performing the request
                dare_platform (int): the target DARE platform (optional - if not provided a default platform is used)
                experiment_group_name (str): the name of the relevant experiment group
                experiment_name (str): the name of the associated experiment
                experiment_dir (str): the path to the experiment
                experiment_group_dir (str): the path to the experiment group
        """
        # check for empty experiment
        experiment_files = os.listdir(experiment_dir)
        if not experiment_files:
            os.rmdir(experiment_dir)
            experiment_group = ExperimentGroup.objects.get(username=username, dare_platform=dare_platform,
                                                           name=experiment_group_name)
            experiment = Experiment.objects.get(username=username, name=experiment_name,
                                                experiment_group=experiment_group)
            group_to_be_used = experiment.group_to_be_used
            experiment.delete()
            # check for empty experiment group
            if group_to_be_used:
                exp_group_files = os.listdir(experiment_group_dir)
                if not exp_group_files:
                    os.rmdir(experiment_group_dir)
                    experiment_group.delete()


class UploadService:
    """
        Class providing backend methods to create/update/retrieve/delete uploaded files in the DARE platform.
    """

    def create_upload(self, username, dare_platform, dataset_name, file, filename, path):
        """
            Creates a new upload object in the DB and stores the file in the Shared File System of the DARE platform

            Args
                username (str): the username of the user performing the request
                dare_platform (int): the target DARE platform (optional - if not provided a default platform is used)
                dataset_name (str): the relevant dataset name (specified to enable DCAT functionality to the DARE
                Knowledge base)
                file (File): the file to be stored in the platform
                path (str): the directory in the uploads/ dir in the platform to store the given file

            Returns
                Upload or str: the new Upload object stored in the DB or an error message
        """
        base_path = join(utils.d4p_sfs['mountpath'], username, utils.UPLOADS)
        # if file and utils.allowed_file(filename):
        filename = secure_filename(filename)
        file_folder = self.__create_file_folder(base_path, path)
        file_path = join(file_folder, filename)
        with open(file_path, "w") as f:
            f.write(file)
        upload = Upload(dare_platform=dare_platform, username=username, dataset_name=dataset_name,
                        name=filename, upload_path=file_folder)
        upload.save()
        return upload
        # else:
        #     return "File extension is not allowed"

    def update_upload(self, username, dare_platform, old_filename, old_path, new_filename, new_folder_name, new_file):
        """
            Updates an Upload object in the DB and the relevant folder/file names in the Shared File System.

            Args
                username (str): the username of the user performing the request
                dare_platform (int): the target DARE platform (optional - if not provided a default platform is used)
                old_filename (str): the old name of the target file
                old_path (str): the old name of the relevant folder
                new_filename (str): the new name of the file
                new_folder_name (str): the new name of the folder
                new_file (bytes): the new uploaded file

            Returns
                str: when an exception occurs, returns an error message
        """
        base_path = join(utils.d4p_sfs['mountpath'], username, utils.UPLOADS)
        old_folder_path = join(base_path, old_path) if old_path else base_path
        try:
            upload = self.get_upload(username, dare_platform, old_filename, old_folder_path)
            if upload:
                old_file_path = join(old_folder_path, old_filename)
                new_folder_path = join(base_path, new_folder_name) if new_folder_name else old_folder_path
                new_file_path = join(new_folder_path, new_filename) if new_filename else join(new_folder_path,
                                                                                              old_filename)
                if new_folder_name:
                    utils.check_create_dirs(base_path, [new_folder_name])
                    upload.upload_path = new_folder_path
                if new_filename:
                    os.remove(old_file_path)
                    upload.name = new_filename
                upload.save()
                with open(new_file_path, "w") as f:
                    f.write(new_file)
                files = os.listdir(old_folder_path)
                if not files:
                    os.rmdir(old_folder_path)
                return upload
        except (EmptyResultSet, ObjectDoesNotExist, MultipleObjectsReturned, Exception) as e:
            return "Upload with name {} in folder {} for user {} was not found".format(old_filename, old_folder_path,
                                                                                       username)

    def delete_upload(self, username, folder, filename, dare_platform):
        """
            Deletes an existing Upload object - the relevant entry in the DB and the relevant file in the SFS
            If only the folder name is provided, the whole folder is removed (and all the relevant entries in the DB),
            otherwise a target file is removed.

            Args
                username (str): the username of the user performing the request
                folder (str): the name of the folder
                filename (str): the name of the file
                dare_platform (int): the target DARE platform (optional - if not provided a default platform is used)
            Returns
                str: status message
        """
        base_path = join(utils.d4p_sfs['mountpath'], username, utils.UPLOADS)
        if not filename and folder:
            # delete specified folder
            self.__delete_folder(base_path, username, dare_platform, folder)
            return "Uploads in folder {} are successfully deleted".format(folder)
        elif filename:
            # delete only file
            self.__delete_file(base_path, folder, username, dare_platform, filename)
            return "Upload with name {} in folder {} is successfully deleted".format(filename, folder)

    @staticmethod
    def get_upload(username, dare_platform, filename, folder_path):
        """
            Retrieves a target upload from the DB

            Args
                username (str): the username of the user performing the request
                dare_platform (int): the target DARE platform (optional - if not provided a default platform is used)
                filename (str): the name of the file
                folder_path (str): the name of the folder

            Returns
                Upload: the target upload from the DB
        """
        return Upload.objects.get(username=username, dare_platform=dare_platform, name=filename,
                                  upload_path=folder_path)

    @staticmethod
    def get_uploads(upload_path, username, dare_platform):
        """
            Retrieves a list of uploads under the same folder for a specific user

            Args
                username (str): the username of the user performing the request
                dare_platform (int): the target DARE platform (optional - if not provided a default platform is used)
                upload_path (str): the name of the folder

            Returns
                list: a list of Upload objects
        """
        return Upload.objects.filter(upload_path=upload_path, username=username, dare_platform=dare_platform)

    @staticmethod
    def get_upload_folders(username, dare_platform):
        """
            Returns all the upload folders of a specific user

            Args
                username (str): the username of the user performing the request
                dare_platform (int): the target DARE platform (optional - if not provided a default platform is used)

            Returns
                list: a list of uploads containing all the upload folders
        """
        return Upload.objects.filter(username=username, dare_platform=dare_platform)

    @staticmethod
    def __create_file_folder(base_path, path):
        """
            Creates a new folder to upload a new file if requested by the user (path is not empty)

            Args
                base_path (str): the path to the uploads/ dir of the user in the DARE platform
                path (str): the name of the new folder
        """
        if path:
            path_parts = path.split("/")
            utils.check_create_dirs(base_path=base_path, dir_names=path_parts)
            return join(base_path, path)
        else:
            return base_path

    def __delete_folder(self, base_path, username, dare_platform, folder):
        """
            Deletes a folder inside the uploads/ dir of the user. All the relevant entries in the DB are also deleted.

            Args
                base_path (str): the path to the uploads/ dir of the user in the DARE platform
                username (str): the username of the user performing the request
                dare_platform (int): the target DARE platform (optional - if not provided a default platform is used)
                folder (str): the name of the folder to be deleted
        """
        # only folder name provided -- delete the folder
        path_to_folder = join(base_path, folder)
        os.rmdir(path_to_folder)
        uploads = self.get_uploads(path_to_folder, username, dare_platform)
        for up in uploads:
            up.delete()

    def __delete_file(self, base_path, folder, username, dare_platform, filename):
        """
            Deletes a file from the SFS and the relevant entry from the DB

            Args
                base_path (str): the path to the uploads/ dir of the user in the DARE platform
                folder (str): the name of the folder where the file is stored. If not provided, it is assumed that the
                file is under the uploads/ directory of the user in the SFS
                username (str): the username of the user performing the request
                dare_platform (int): the target DARE platform (optional - if not provided a default platform is used)
                filename (str): the name of the file to be deleted
        """
        folder_path = join(base_path, folder) if folder else base_path
        upload = self.get_upload(username, dare_platform, filename, folder_path)
        os.remove(join(folder_path, secure_filename(filename)))

        # delete directory if empty
        if folder:
            files = os.listdir(folder_path)
            if not files:
                os.rmdir(folder_path)
        # delete upload from DB
        upload.delete()


class DownloadService:
    """
        Class providing backend methods to download files from the SFS of the DARE platform and also retrieve a full
        path to a file or folder
    """

    def get_download_path(self, username, dare_platform, experiment_group_name, experiment_name, run_name,
                          filename):
        """
            Provides the path to a file to be downloaded. The file should be in an execution directory

            Args
                username (str): the username of the user performing the request
                dare_platform (int): the target DARE platform (optional - if not provided a default platform is used)
                experiment_group_name (str): the name of the associated experiment group
                experiment_name (str): the name of the associated experiment
                run_name (str): the name of the target run
                filename (str): the name of the file to be downloaded

            Returns
                str: the path to the target file
        """
        base_path = join(utils.d4p_sfs['mountpath'], username, utils.EXPERIMENTS)
        run_service = RunService()
        run = run_service.get_run(username, dare_platform, experiment_group_name, experiment_name, run_name)
        path = self.__get_output_folder_path(base_path, experiment_group_name, experiment_name, run)
        return join(path, filename)

    @staticmethod
    def get_full_path(kind, username, names):
        """
            Returns the full path to a target file/folder in the SFS of the user

            Args
                kind (str): official or trial
                username (str): the username of the user performing the request
                names (str): a list with all the directory names in the path

            Returns:
                str: the full path to a file
        """
        path = join(utils.d4p_sfs["mountpath"], username)
        if kind == utils.EXPERIMENTS.lower():
            path = join(path, utils.EXPERIMENTS)
        elif kind == utils.UPLOADS.lower():
            path = join(path, utils.UPLOADS)
        if names:
            for name in names:
                if name:
                    path = join(path, name)
            return path

    @staticmethod
    def __get_output_folder_path(base_path, experiment_group_name, experiment_name, run):
        """
            Concatenates the path to the execution directory with the output dir, i.e.
            /home/mpiuser/sfs/user1/experiments/Group1/Experiment1/user1_of_20234984_Run1/output/

            Args
                base_path (str): the path to the experiments/ directory of the user
                experiment_group_name (str): the name of the associated experiment group
                experiment_name (str): the name of the associated experiment
                run (Run): the target Run object
        """
        return join(base_path, experiment_group_name, experiment_name, run.run_dir,
                    utils.OUTPUT) if run.experiment.group_to_be_used else \
            join(base_path, experiment_name, run.run_dir, utils.OUTPUT)
