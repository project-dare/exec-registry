from django.contrib.auth.models import User
from rest_framework import serializers
from registry.models import *


class RunSerializer(serializers.ModelSerializer):
    """
    Class used to serialize a Run object. Contains all the declared fields in the models.py for the Run class.
    """
    class Meta:
        model = Run
        fields = ["id", "username", "experiment", "date", "name", "kind", "run_dir"]
        depth = 1


class ExperimentSerializer(serializers.ModelSerializer):
    """
    Class used to serialize an Experiment object. All the declared fields in the models.py are included in this
    serializer.
    """
    runs = RunSerializer(source='get_runs', many=True, read_only=True)

    class Meta:
        model = Experiment
        fields = ["id", "name", "username", "experiment_group", "group_to_be_used", "runs"]
        depth = 1


class ExperimentGroupSerializer(serializers.ModelSerializer):
    """
    Class used to serialize an ExperimentGroup object. All the declared fields in the models.py are included in this
    serializer.
    """
    experiments = ExperimentSerializer(source='get_experiments', many=True, read_only=True)

    class Meta:
        model = ExperimentGroup
        fields = ["id", "name", "username", "dare_platform", "experiments"]
        depth = 1


class UploadSerializer(serializers.ModelSerializer):
    """
    Serializer for Upload object using all the fields from the models.Upload class
    """
    class Meta:
        model = Upload
        fields = ["id", "dare_platform", "username", "dataset_name", "name", "upload_path"]
        depth = 1


class StringListField(serializers.ListField):
    """
    Serializer for a list of strings.
    """
    child = serializers.CharField()


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ["username", "email"]
