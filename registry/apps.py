import platform
from os import mkdir, getlogin
from os.path import exists, join

from django.apps import AppConfig
from kubernetes.config.config_exception import ConfigException

from registry import utils


class RegistryConfig(AppConfig):
    name = 'registry'
    verbose_name = "Execution registry"

    def ready(self):
        """
            Function that is executed only on startup. Initializes the d4p and specfem data from the respective yaml
            files. The most important information is the mount path of the two execution contexts. If the configuration
            loading fails the function will generate a custom path to store the execution experiments based on the
            underlying operating system.
        """

    try:
        utils.d4p_sfs, utils.specfem_sfs = utils.init_sfs_data()
    except (ConfigException, Exception):
        operating_system_name = platform.system()
        path = ""
        if operating_system_name.lower() == "linux":
            try:
                if not exists(join("/home", getlogin(), "mpiuser")):
                    mkdir(join("/home", getlogin(), "mpiuser"))
                    mkdir(join("/home", getlogin(), "mpiuser", "sfs"))
                path = join("/home", getlogin(),"mpiuser", "sfs")
            except (OSError, Exception):
                mkdir("/mpiuser")
                mkdir("/mpiuser/sfs")
                path = "/mpiuser/sfs"
        elif operating_system_name.lower() == "windows":
            if not exists(join("C:\\Users", getlogin(), "mpiuser")):
                mkdir(join("C:\\Users", getlogin(), "mpiuser"))
                mkdir(join("C:\\Users", getlogin(), "mpiuser", "sfs"))
            path = join("C:\\Users", getlogin(), "mpiuser", "sfs")
        utils.d4p_sfs = {"mountpath": path}
