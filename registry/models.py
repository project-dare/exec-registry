import json

from django.db import models
from django.forms.models import model_to_dict


# Create your models here.
class ExperimentGroup(models.Model):
    """
    Django model that represents experiment groups. This model is optional for the API, a default experiment group is
    generated and used for all experiments that do not define a group. Apart from the experiment group name
    and the user associated with it, the model keeps also the information regarding the specific dare platform
    where the data are stored.
    """
    class Meta:
        """
        Keeps information on the database table (i.e. the table name, the indices etc)
        """
        db_table = "experiment_groups"
        indexes = [
            models.Index(fields=['username'])
        ]

    id = models.AutoField(primary_key=True, unique=True)
    name = models.CharField(max_length=255)
    username = models.CharField(max_length=255)
    dare_platform = models.IntegerField()

    def get_experiments(self):
        """
        Method to get all the experiments from the DB based on the experiment group

        Returns
            list: all the experiments associated with the specific experiment group object instance
        """
        return Experiment.objects.filter(experiment_group=self)

    def __str__(self):
        """
        Overrides the __str__ object method to show all the object fields & values as strings.

        Returns
            str: string containing all the class fields and their values
        """
        obj = model_to_dict(self)
        return json.dumps(obj)


class Experiment(models.Model):
    """
    Django model that represents an experiment. Each experiment is associated with one Experiment Group and multiple
    executions / runs. The name of an experiment should be unique in the Experiment Group that contains the experiment,
    but can be reused in different experiment groups. The field group_to_be_used is used in order to decide whether
    a default experiment group should be generated or if it is expected by the user to associate the experiment with
    an already existing experiment group.
    """
    class Meta:
        """
        Class Meta keeps information on the experiments table (table name, indices)
        """
        db_table = "experiments"
        indexes = [
            models.Index(fields=['username'])
        ]

    id = models.AutoField(primary_key=True, unique=True)
    name = models.CharField(max_length=255)
    username = models.CharField(max_length=255)
    group_to_be_used = models.BooleanField()
    experiment_group = models.ForeignKey(ExperimentGroup, related_name="experiments", on_delete=models.CASCADE)

    def get_runs(self):
        """
        Method to retrieve all the runs associated with a specific experiment

        Returns
            list: list of run objects associated with the specific experiment object instance.
        """
        return Run.objects.filter(experiment=self)

    def __str__(self):
        """
        Method to transform an experiment object to string so as to keep all the field values as string.

        Returns
            str: string with all the object information
        """
        obj = model_to_dict(self)
        return json.dumps(obj)


class Run(models.Model):
    """
    Django model that represents a specific experiment's execution. Keeps information such as the name of the run,
    the associated experiment, the timestamp of the execution and the exact run directory name (concatenation of the
    username, the date and the run name.
    """
    TRIAL = "TR"
    OFFICIAL = "OF"
    KIND_CHOICES = [(TRIAL, "trial"), (OFFICIAL, "official")]

    class Meta:
        """
        Class Meta keeps information for the runs table in the DB
        """
        db_table = "runs"
        indexes = [
            models.Index(fields=['username'])
        ]

    id = models.AutoField(primary_key=True, unique=True)
    username = models.CharField(max_length=255)
    experiment = models.ForeignKey(Experiment, related_name="runs", on_delete=models.CASCADE)
    date = models.DateTimeField()
    name = models.CharField(max_length=255)
    kind = models.CharField(max_length=2, choices=KIND_CHOICES, default=TRIAL)
    run_dir = models.CharField(max_length=500)

    def __str__(self):
        """
        toString-like method to transform a Run object to a printable format.

        Returns
            str: the object as string
        """
        obj = model_to_dict(self)
        obj['date'] = obj['date'].strftime("%Y-%m-%d %H:%M:%S")
        return json.dumps(obj)


class Upload(models.Model):
    """
    Django model that represents a file that is uploaded in the DARE platform. Information on the specific dare
    platform that the file is uploaded, the username, the dataset name, the name of the file and a specific directory
    is kept in this model.
    """
    class Meta:
        """
        Class Meta keeps information on the uploads DB table
        """
        db_table = "uploads"
        indexes = [
            models.Index(fields=['username'])
        ]
    id = models.AutoField(primary_key=True, unique=True)
    dare_platform = models.IntegerField()
    username = models.CharField(max_length=255)
    dataset_name = models.CharField(max_length=500)
    name = models.CharField(max_length=255)
    upload_path = models.CharField(max_length=2000)

    def __str__(self):
        """
        toString-like method to make the Upload object printable

        Returns
            str: the object in string format
        """
        obj = model_to_dict(self)
        return json.dumps(obj)
