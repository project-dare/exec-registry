import logging
import os
from os.path import join, exists

import requests
import yaml
from kubernetes import client, config
from kubernetes.config.config_exception import ConfigException

EXPERIMENTS = "experiments"
UPLOADS = "uploads"
OUTPUT = "output"
DEFAULT_EXPERIMENT_GROUP = "Group1"
DEFAULT_DATASET = "Dataset1"
DEFAULT_DARE_PLATFORM = 0
d4p_sfs, specfem_sfs = {}, {}
logger = logging.getLogger(__name__)


def init_sfs_data():
    """
        Function that accesses the Kubernetes yaml file for the exec-registry component. Gets all the information
        related to the mount points of d4p and specfem and stores it in the respective variables.
    """
    try:
        r = requests.get(
            'https://gitlab.com/project-dare/dare-platform/-/raw/master/k8s/exec-registry-dp.yaml')
        name_space = yaml.safe_load(r.text)['metadata']['namespace']
        # Current pod running flask api
        target_pod = init_from_yaml(name_space)
        # Namespace found in yaml, but not in kubernetes
        if target_pod is None:
            name_space = 'default'
            # Current pod running flask api
            target_pod = init_from_yaml(name_space)
    # No namespace in yaml
    except (ConfigException, Exception):
        name_space = 'default'
        # Current pod running flask api
        target_pod = init_from_yaml(name_space)

    d4p_config = {'jobname': 'd4p-openmpi', 'mountname': target_pod.spec.containers[0].volume_mounts[0].name,
                  'mountpath': target_pod.spec.containers[0].volume_mounts[0].mount_path,
                  'volname': target_pod.spec.volumes[0].name,
                  'fsname': target_pod.spec.volumes[0].flex_volume.options['fsName']}
    logger.info("Base mount path for d4p: {}".format(d4p_config["mountpath"]))

    specfem_config = {'specname': 'specfem-openmpi',
                      'specvolname': target_pod.spec.containers[0].volume_mounts[1].name,
                      'spec_mountpath': target_pod.spec.containers[0].volume_mounts[1].mount_path,
                      'fsname': target_pod.spec.volumes[0].flex_volume.options['fsName']}
    return d4p_config, specfem_config


# Return $HOSTNAME pod (current pod running this code)
def init_from_yaml(name_space):
    """
        Function that loads the kubernetes configuration based on the pod configuration. Finds the exec-registry
        hostname and returns it.

        Args:
            name_space (str): the namespace under which the dare-api is deployed in Kubernetes

        Returns:
            str: the hostname
    """
    # Rbac authorization for inter container kubectl api calls
    config.load_incluster_config()
    # Init client
    v1 = client.CoreV1Api()
    # Get pods in dare-api namespace
    ret = v1.list_namespaced_pod(name_space)
    # Find exec-registry pod
    for i in ret.items:
        if i.metadata.name == os.environ['HOSTNAME']:
            return i


def check_create_dirs(base_path, dir_names):
    """
        Function to create all the necessary directories in a path. The base_path and dir_names should be provided.
        E.g. base_path = "/home/mpiuser/sfs/" and dir_names = ["username", "experiments", "experiment1"] and after the
        first loop the base_path will be "/home/mpiuser/sfs/username" and will continue with the second list item
        which is the experiments.

        Args:
            base_path (str): the starting point in the directories (should already exist).
            dir_names (list): list with the directory hierarchy that should be created in the file system
    """
    for dir_name in dir_names:
        if not exists(join(base_path, dir_name)):
            os.mkdir(join(base_path, dir_name))
        base_path = join(base_path, dir_name)


def allowed_file(filename, allowed_extensions=('zip', 'rar')):
    """
        Checks if the file that the user requested to upload has any of the allowed file extensions. So far, the only
        allowed extension is the .zip extension

        Args:
            filename (str): the name of the file to be uploaded in the platform
            allowed_extensions (tuple): list containing the allowed file extensions

        Returns:
            bool: True/False based on whether the file's extension is allowed
    """
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in allowed_extensions
