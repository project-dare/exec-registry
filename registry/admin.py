from django.contrib import admin
from registry.models import *

# Register your models here.
admin.site.register(Run)
admin.site.register(Experiment)
admin.site.register(ExperimentGroup)
admin.site.register(Upload)
