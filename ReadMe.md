# Exec-registry
## **IMPORTANT**

This repository is obsolete. Exec-registry is merged with Exec-api and can be found in the exec-api repository in the dev branch.
Needs to be tested and merged to the master branch.

The dev branch contains also the Dockerfile & scripts for the new exec-api image as well as the k8s files.