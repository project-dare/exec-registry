Utility methods
==================

.. toctree::
    :caption: Utility methods

.. automodule:: registry.utils
   :members: