Django views (RESTful API)
==========================

.. toctree::
        permissions
        services


.. automodule:: registry.views
   :members: